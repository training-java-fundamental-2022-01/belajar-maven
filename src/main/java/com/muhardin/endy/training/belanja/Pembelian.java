package com.muhardin.endy.training.belanja;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class Pembelian {
    private Pelanggan pelanggan;
    private LocalDateTime waktuTransaksi;
    private List<DetailPembelian> daftarDetailPembelian = new ArrayList<>();
    private StatusPembelian statusPembelian = StatusPembelian.BELUM_DIBAYAR;

    public BigDecimal total(){
        BigDecimal nilaiTotal = BigDecimal.ZERO;

        for(DetailPembelian dp : daftarDetailPembelian) {
            nilaiTotal = nilaiTotal.add(dp.subtotal());
        }

        return nilaiTotal;
    }

    public List<DetailPembelian> getDaftarDetailPembelian(){
        return this.daftarDetailPembelian;
    }

    public void setDaftarDetailPembelian(List<DetailPembelian> daftar) {
        this.daftarDetailPembelian = daftar;
    }
}

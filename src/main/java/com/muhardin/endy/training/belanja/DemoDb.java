package com.muhardin.endy.training.belanja;

import com.muhardin.endy.training.belanja.db.ProdukDb;

public class DemoDb {
    public static void main(String[] args) {
        ProdukDb produkDb = new ProdukDb();

        for(Produk p : produkDb.semuaProduk()) {
            System.out.println("Data Produk : "+p);
        }
    }
}

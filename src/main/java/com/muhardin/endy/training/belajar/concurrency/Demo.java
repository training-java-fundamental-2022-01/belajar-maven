package com.muhardin.endy.training.belajar.concurrency;

public class Demo {
    public static void main(String[] args) throws InterruptedException {
        IncrementNonConcurrent inc1 = new IncrementNonConcurrent("INC-1");
        IncrementNonConcurrent inc2 = new IncrementNonConcurrent("INC-2");
        IncrementNonConcurrent inc3 = new IncrementNonConcurrent("INC-3");

        inc1.start();
        inc2.start();
        inc3.start();

        IncrementConcurrent ic1 = new IncrementConcurrent("IC-1");
        IncrementConcurrent ic2 = new IncrementConcurrent("IC-2");
        IncrementConcurrent ic3 = new IncrementConcurrent("IC-3");

        ic1.start();
        ic2.start();
        ic3.start();
    }
}

package com.muhardin.endy.training.belanja;

import java.math.BigDecimal;

public class DiskonProduk implements Diskon {
    private static final String MEREK_DISKON = "Samsung";
    private static final BigDecimal PERSENTASE_DISKON = new BigDecimal(0.2);

    @Override
    public BigDecimal hitung(Pembelian p) {
        BigDecimal totalDiskon = BigDecimal.ZERO;

        //for(int x=0; x<p.getDaftarDetailPembelian().size(); x++){
        for(DetailPembelian d : p.getDaftarDetailPembelian()) {
            if(MEREK_DISKON.equalsIgnoreCase(d.getProduk().getMerek())){
                totalDiskon = totalDiskon.add(PERSENTASE_DISKON.multiply(d.subtotal()));
            }
        }

        return totalDiskon;
    }
    
}

package com.muhardin.endy.training.belanja;

import java.math.BigDecimal;

public interface Diskon {
    BigDecimal hitung(Pembelian p);
}

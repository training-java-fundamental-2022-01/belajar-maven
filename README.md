# Belajar Maven #

Fungsi / kegunaan Maven

* Struktur folder project
* Project portability
* Dependency Management

## Membuat Maven Project ##

1. Instalasi dengan mengikuti panduan [di sini](https://software.endy.muhardin.com/java/persiapan-coding-java/)
2. Membuat struktur folder sesuai aturan Maven
3. Membuat `pom.xml`

## Perintah-perintah Maven ##

Perintah Maven ini dijalankan dalam folder yang ada `pom.xml`nya.

1. Compile dan membuat file `jar`

    ```
    mvn package
    ```

2. Menghapus/membersihkan hasil compile sebelumnya

    ```
    mvn clean
    ```

3. Menghapus, lalu melakukan compile ulang

    ```
    mvn clean package
    ```

4. Menjalankan `main` class

    ```
    mvn clean package exec:java -Dexec.mainClass="com.muhardin.endy.training.belajar.maven.HaloMaven"
    ```

## Struktur Aplikasi Java ##

[![Struktur Aplikasi Java](img/struktur-aplikasi-java.png)](img/struktur-aplikasi-java.png)

[![Konsep OOP](img/konsep-oop.jpg)](img/konsep-oop.jpg)

## Contoh Kasus ##

Transaksi Belanja Online

Class-class yang ada dalam aplikasi :

### Domain Model ###

- Customer
- Produk
- Keranjang
- Pembelian
- Tagihan
- Pembayaran
- Pengiriman

Data-data dalam masing-masing class

* Pelanggan -> class

    * nama : String
    * email : String
    * alamat : String
    * nomorHandphone : String

* Produk

    * merek : String
    * tipe : String
    * deskripsi : String
    * foto : String (lokasi penyimpanan), byte[]
    * harga : BigDecimal

* Pembelian

    * pelanggan
    * waktu pembelian : DateTime
    * daftar produk yang dibeli (DetailPembelian) : List / Array

* Detail Pembelian

    * produk 
    * jumlah : integer


## Tipe Data ##

1. Numerik

    * Bilangan bulat : byte, short, int, long
    * Bilangan pecahan : float, double, BigDecimal

2. Alfanumerik : huruf dan angka

    * Character
    * String

3. Boolean : true dan false

4. Waktu

    * DateTime
    * Date
    * Time

## Primitive vs Reference Type ##

* Contoh primitive :

    ```java
    int a = 10;
    ```

* Contoh reference / class

    ```java
    Integer a = new Integer(10);
    ```

* Autoboxing

    ```java
    // harusnya seperti ini
    Integer a = new Integer(10); 

    // tapi boleh ditulis seperti ini
    Integer a = 10;
    ```

* Perbedaan primitive vs reference

    ```java
    int a;
    Integer x;
    ```

    Bila tidak diisi nilai, maka:

        * `a` : nol
        * `x` : `null`

## Latihan ##

1. Class Pelanggan
   - buat getter dan setter

2. Buat object Pelanggan di class Demo
3. Isikan object Pelanggan ke object Pembelian

# Referensi #

* [Java Tutorial Resmi](https://docs.oracle.com/javase/tutorial/)
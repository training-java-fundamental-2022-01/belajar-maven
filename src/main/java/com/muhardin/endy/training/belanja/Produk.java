package com.muhardin.endy.training.belanja;

import java.math.BigDecimal;

import lombok.ToString;

@ToString
public class Produk {
    private String merek;
    private String tipe;
    private String deskripsi;
    private byte[] foto;
    private BigDecimal harga;

    // default constructor
    // dibuatkan otomatis oleh Java 
    // kalau kita tidak mendefinisikan constructor lain
    public Produk() {

    }

    // kalau sudah membuat constructor sendiri
    // maka default constructor tidak akan dibuatkan
    public Produk(String merek, String tipe, BigDecimal harga){
        this.merek = merek;
        this.tipe = tipe;
        this.harga = harga;
    }

    public String getMerek(){
        return this.merek;
    }

    public void setMerek(String m){
        merek = m;
    }

    public String getTipe(){
        return this.tipe;
    }

    public void setTipe(String tipe){
        this.tipe = tipe;
    }

    public String getDeskripsi(){
        return this.deskripsi;
    }

    public void setDeskripsi(String m){
        this.deskripsi = m;
    }

    public byte[] getFoto(){
        return this.foto;
    }

    public void setFoto(byte[] foto){
        this.foto = foto;
    }

    public BigDecimal getHarga(){
        return this.harga;
    }

    public void setHarga(BigDecimal harga){
        Boolean lebihDariNol = BigDecimal.ZERO.compareTo(harga) == -1;
        if(lebihDariNol) {
            this.harga = harga; // dijalankan bila lebihDariNol isinya true
        } else {
            this.harga = BigDecimal.ZERO;  // dijalankan bila lebihDariNol isinya false
        }
        
    }
}

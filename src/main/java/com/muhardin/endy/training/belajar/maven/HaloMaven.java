package com.muhardin.endy.training.belajar.maven;

public class HaloMaven {
    public static void main(String[] args) {

        // statement hanya boleh dalam method
        System.out.println("Halo Maven");

        Integer x = 5;
        Integer y = 10;

        System.out.println("X + Y = "+(x + y));

        String a = "3";
        String b = "2";
        System.out.println("A + B = "+(a + b));

        Integer cobaParsing1 = Integer.valueOf("7");
        System.out.println("Coba parsing string ke integer : "+cobaParsing1);

        try {
            Integer cobaParsing2 = Integer.valueOf("a");
            System.out.println("Coba parsing string ke integer : "+cobaParsing2);
        } catch (NumberFormatException err) {
            System.out.println("Gagal parsing string ");
            System.out.println("Pesan error : "+err.getMessage());
        }
    }

    // tidak boleh taruh statement di sini
    //System.out.println("Test");

    // inner class (class dalam class)
    class Coba {

    }
}

// tidak boleh taruh statement di sini
//System.out.println("Test");
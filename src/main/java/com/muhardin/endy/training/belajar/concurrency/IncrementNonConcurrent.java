package com.muhardin.endy.training.belajar.concurrency;

public class IncrementNonConcurrent {
    private String nama;

    public IncrementNonConcurrent(String nama){
        this.nama = nama;
    }

    public void start() throws InterruptedException {
        Integer max = 5;

        for(int i = 0; i<max; i++){
            System.out.println(nama + " - i : "+i);
            Thread.sleep(1000);
        }
    }
}

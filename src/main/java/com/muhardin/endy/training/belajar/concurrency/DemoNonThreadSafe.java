package com.muhardin.endy.training.belajar.concurrency;

import java.util.ArrayList;
import java.util.List;

public class DemoNonThreadSafe {
    public static void main(String[] args) throws InterruptedException {
        List<TambahCounter> daftarThreadTambahCounter = new ArrayList<>();
        List<KurangCounter> daftarThreadKurangCounter = new ArrayList<>();
        Counter c = new Counter();

        Integer jumlahThread = 1000;
        for(int i=0; i<jumlahThread; i++){
            TambahCounter tc1 = new TambahCounter("TambahCounter"+i, c);
            daftarThreadTambahCounter.add(tc1);
            
            KurangCounter kc1 = new KurangCounter("KurangCounter"+i, c);
            daftarThreadKurangCounter.add(kc1);    
        }

        // start thread
        for(int i=0; i<jumlahThread; i++){
            daftarThreadTambahCounter.get(i).start();
            daftarThreadKurangCounter.get(i).start();
        }

        // tunggu semua thread selesai
        for(int i=0; i<jumlahThread; i++){
            daftarThreadTambahCounter.get(i).join();
            daftarThreadKurangCounter.get(i).join();
        }

        System.out.println("=======");
        c.show();
    }

    static class TambahCounter extends Thread {
        private String nama;
        private Counter c;

        public TambahCounter(String nama, Counter c){
            this.nama = nama;
            this.c = c;
        }

        public void run(){
            try {
                for(int i=0; i<100; i++) {
                    System.out.println(nama+" increment");
                    // sinkronisasi objectnya
                    //synchronized(c) {
                        c.increment();
                        c.show();
                    //}
                    //Thread.sleep(1000);
                }
            } catch (Exception err) {
                err.printStackTrace();
            }
        }
    }

    static class KurangCounter extends Thread {
        private String nama;
        private Counter c;

        public KurangCounter(String nama, Counter c){
            this.nama = nama;
            this.c = c;
        }

        public void run(){
            try {
                for(int i=0; i<100; i++) {
                    System.out.println(nama+" decrement");
                    c.decrement();
                    c.show();
                    //Thread.sleep(1000);
                }
            } catch (Exception err) {
                err.printStackTrace();
            }
        }
    }
}

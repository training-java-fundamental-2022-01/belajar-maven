package com.muhardin.endy.training.belanja.db;

import java.io.BufferedWriter;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;

import com.muhardin.endy.training.belanja.DetailPembelian;
import com.muhardin.endy.training.belanja.Pembelian;

public class PembelianDb {
    private static final String NAMA_FILE_PEMBELIAN = "pembelian.txt";

    public void simpan(Pembelian pembelian) {
        try {
            Charset charset = Charset.forName("UTF-8");
            URI file = ClassLoader.getSystemResource(NAMA_FILE_PEMBELIAN).toURI();
            System.out.println("File output : "+file.toString());
            Path lokasiFile = Path.of(file);
            BufferedWriter writer = Files.newBufferedWriter(lokasiFile, charset, StandardOpenOption.WRITE);
            for(DetailPembelian dp : pembelian.getDaftarDetailPembelian()) {
                String data = dp.getProduk().getMerek()
                +","
                +dp.getProduk().getTipe()
                +","
                +dp.getProduk().getHarga()
                +","
                +dp.getJumlah()
                +"\r\n";
                writer.write(data);
            }
            
            writer.close();
        } catch (IOException | URISyntaxException err) {
            System.out.println("Terjadi error menulis data : "+err.getMessage());
        }
    }
}

package com.muhardin.endy.training.belajar.concurrency;

import java.util.concurrent.atomic.AtomicInteger;

public class AtomicCounter {
    //private Integer x = 0;
    private AtomicInteger y = new AtomicInteger(0);

    public void increment(){
        //this.x++;
        y.incrementAndGet();
    }

    public void decrement(){
        //this.x--;
        y.decrementAndGet();
    }

    public void show(){
        //System.out.println("Nilai X : "+x);
        System.out.println("Nilai Y : "+y.get());
    }
}

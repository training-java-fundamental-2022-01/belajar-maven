package com.muhardin.endy.training.belajar.concurrency;

public class SynchronizedCounter {
    private Integer x = 0;

    public synchronized void increment(){
        this.x++;
    }

    public synchronized void decrement(){
        this.x--;
    }

    public void show(){
        System.out.println("Nilai X : "+x);
    }
}

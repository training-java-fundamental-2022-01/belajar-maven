package com.muhardin.endy.training.belanja;

import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class DetailPembelian {
    private Produk produk;
    private Integer jumlah;

    public BigDecimal subtotal(){
        return produk.getHarga().multiply(new BigDecimal(jumlah));
    }
}
